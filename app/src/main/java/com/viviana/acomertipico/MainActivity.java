package com.viviana.acomertipico;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.viviana.acomertipico.adapters.ExampleAdapter;

import java.util.List;

import interfases.ApiService;
import models.categorias.Category;
import models.categorias.ResponseCategory;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    Button btnSig, btnSiguiente, btnsigui, btnRecipe;
    RecyclerView rvExample;


    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Obtener();
        btnSig = findViewById(R.id.btnSig);
        btnSiguiente = findViewById(R.id.btnsiguiente);
        rvExample = findViewById(R.id.rvExample);
        btnsigui = findViewById(R.id.btnsigui);
        btnRecipe = findViewById(R.id.btnRecipe);
        btnRecipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ir = new Intent(MainActivity.this,Recipe.class);
                startActivity(ir);
            }
        });


        btnSiguiente.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent ir = new Intent(MainActivity.this,Establishment.class);
                startActivity(ir);
            }
        });


        btnSig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ir = new Intent(MainActivity.this,Categoria.class);
                startActivity(ir);
            }
        });
        btnsigui.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ir = new Intent(MainActivity.this,Establishment.class);
                startActivity(ir);
            }
        });
        btnSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ir = new Intent(MainActivity.this,Categoria.class);
                startActivity(ir);
            }
        });
    }
    private void Obtener() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://acomertipico.herokuapp.com/api/")
                .addConverterFactory(GsonConverterFactory.create()).build();

        ApiService Service = retrofit.create(ApiService.class);

       Service.getCategory().enqueue(new Callback<List<ResponseCategory>>() {

           @Override
           public void onResponse(Call<List<ResponseCategory>> call, Response<List<ResponseCategory>> response) {
               Toast.makeText(getApplicationContext(), response.body().toString(), Toast.LENGTH_SHORT).show();


               rvExample.setAdapter(new ExampleAdapter(response.body()));
               rvExample.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
           }

           @Override
           public void onFailure(Call<List<ResponseCategory>> call, Throwable t) {
               Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_LONG).show();
               Log.e("onFailure: ",t.toString() );
           }
       });

        }
}
