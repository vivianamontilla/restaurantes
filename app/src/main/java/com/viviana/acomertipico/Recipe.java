package com.viviana.acomertipico;

import static android.widget.Toast.*;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import interfases.ApiService;
import models.recetas.ResponseRecipe;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Recipe extends AppCompatActivity {
    EditText edtname,edtimage,edtlink,edtDescripcion;
    ImageView imgbarra, imgpuntos;
    Button btnActualizar;
    String name;
    String link;
    String description;
    String image;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe);
            obtener();
    }

    private void obtener() {
        edtname= findViewById(R.id.edtname);
        edtimage = findViewById(R.id.edtimage);
        imgbarra = findViewById(R.id.imgbarra);
        imgpuntos = findViewById(R.id.imgpuntos);
        edtlink = findViewById(R.id.edtlink);
        edtDescripcion = findViewById(R.id.edtdescripcion);
        btnActualizar = findViewById(R.id.btnActualizar);

        btnActualizar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                name = edtname.getText().toString();
                link = edtlink.getText().toString();
                description = edtDescripcion.getText().toString();
                image = edtimage.getText().toString();


        obtenerPost();
    }
    private void obtenerPost() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://acomertipico.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService service = retrofit.create(ApiService.class);

        service.createRecipe(new models.recetas.Recipe(name, link, description, image)).enqueue(new Callback<ResponseRecipe>() {
            @Override
            public void onResponse(Call<ResponseRecipe> call, Response<ResponseRecipe> response) {
                makeText(Recipe.this, "mensaje enviado"+name, LENGTH_SHORT).show();
                makeText(Recipe.this, "mensaje enviado"+link, LENGTH_SHORT).show();
                makeText(Recipe.this, "mensaje enviado"+description, LENGTH_SHORT).show();
                makeText(Recipe.this, "mensaje enviado"+image, LENGTH_SHORT).show();
                }

            @Override
            public void onFailure(Call<ResponseRecipe> call, Throwable t) {
               Toast.makeText(Recipe.this, "Error"+t, Toast.LENGTH_SHORT).show();


            }
        });
    }
    });
    }
}