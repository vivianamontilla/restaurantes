package com.viviana.acomertipico;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.viviana.acomertipico.adapters.ExampleAdapter;

import java.util.List;

import interfases.ApiService;
import models.categorias.ResponseCategory;
import models.establecimiento.ResponseEstablishment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Establishment extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_establishment);
      obtenerGet();


        }

    private void obtenerGet() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://acomertipico.herokuapp.com/api/")
                .addConverterFactory(GsonConverterFactory.create()).build();


        ApiService service = retrofit.create(ApiService.class);
        service.getEstablishment().enqueue(new Callback<List<ResponseEstablishment>>() {

            @Override
            public void onResponse(Call<List<ResponseEstablishment>> call, Response<List<ResponseEstablishment>> response) {
                Toast.makeText(getApplicationContext(), response.body().toString(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<List<ResponseEstablishment>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_SHORT).show();

            }


        });
    }
}