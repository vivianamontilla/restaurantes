package com.viviana.acomertipico;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.viviana.acomertipico.adapters.ExampleAdapter;

import interfases.ApiService;
import models.categorias.Category;
import models.categorias.ResponseCategory;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Categoria extends AppCompatActivity {
    EditText edtCategoria;
    Button btnEnviar;
    ImageView imgmenu, imgtres;
    String nombre_categoria;


    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categoria);

      edtCategoria = findViewById(R.id.edtCategoria);
      imgmenu = findViewById(R.id.imgmenu);
      imgtres = findViewById(R.id.imgtres);
      btnEnviar = findViewById(R.id.btnEnviar);

      btnEnviar.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              nombre_categoria = edtCategoria.getText().toString();

              obtenerPost();
          }
      });
    }
    private void obtenerPost() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://acomertipico.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiService Service = retrofit.create(ApiService.class);
        Service.crearCategory(new Category(nombre_categoria)).enqueue(new Callback<ResponseCategory>() {
            @Override
            public void onResponse(Call<ResponseCategory> call, Response<ResponseCategory> response) {
                Toast.makeText(Categoria.this,"categoria" + response.body().toString(),Toast.LENGTH_SHORT).show();

                Log.e("onResponse",response.body().toString());
            }

            @Override
            public void onFailure(Call<ResponseCategory> call, Throwable t) {
                Log.e("onFauilure",t.toString());}
        });
    }
}

