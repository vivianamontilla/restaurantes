package com.viviana.acomertipico.adapters;

import android.app.Application;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.viviana.acomertipico.R;

import java.util.ArrayList;
import java.util.List;

import models.categorias.ResponseCategory;

public class ExampleAdapter extends RecyclerView.Adapter<View_Holder> {

    List<ResponseCategory> list = new ArrayList<ResponseCategory>();

    public ExampleAdapter(List<ResponseCategory> data) {
        this.list = data;
    }


    @NonNull
    @Override
    public View_Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_prueba, parent, false);
        View_Holder holder = new View_Holder(v);
        return holder;    }

    @Override
    public void onBindViewHolder(@NonNull View_Holder holder, int position) {
        holder.name.setText(list.get(position).getMessage());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}

