package com.viviana.acomertipico.adapters;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.viviana.acomertipico.R;

public class View_Holder extends RecyclerView.ViewHolder {

    TextView name;

    View_Holder(View itemView) {
        super(itemView);
        name = (TextView) itemView.findViewById(R.id.tvPrueba);
    }
}
