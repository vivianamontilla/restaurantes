package models.establecimiento;

import java.util.List;

import models.categorias.ResponseCategory;

public class Establishment {
     String message;
     String info;
    List<ResponseEstablishment> establishment;

    public Establishment(String message, String info, List<ResponseEstablishment> establishment) {
        this.message = message;
        this.info = info;
        this.establishment = establishment;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public List<ResponseEstablishment> getEstablishment() {
        return establishment;
    }

    public void setEstablishment(List<ResponseEstablishment> establishment) {
        this.establishment = establishment;
    }

    @Override
    public String toString() {
        return "Establishment{" +
                "message='" + message + '\'' +
                ", info='" + info + '\'' +
                ", establishment=" + establishment +
                '}';
    }
}
