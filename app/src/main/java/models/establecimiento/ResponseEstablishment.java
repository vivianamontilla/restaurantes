package models.establecimiento;

import android.content.Intent;

public class ResponseEstablishment {
    Intent id;
            String nombre_establecimiento;
            String descripcion;
            Intent direccion_establecimiento;
            Intent telefono_establecimiento;
            String imagen;
            String ubicacion;

    public ResponseEstablishment(Intent id, String nombre_establecimiento, String descripcion, Intent direccion_establecimiento, Intent telefono_establecimiento, String imagen, String ubicacion) {
        this.id = id;
        this.nombre_establecimiento = nombre_establecimiento;
        this.descripcion = descripcion;
        this.direccion_establecimiento = direccion_establecimiento;
        this.telefono_establecimiento = telefono_establecimiento;
        this.imagen = imagen;
        this.ubicacion = ubicacion;
    }

    public Intent getId() {
        return id;
    }

    public void setId(Intent id) {
        this.id = id;
    }

    public String getNombre_establecimiento() {
        return nombre_establecimiento;
    }

    public void setNombre_establecimiento(String nombre_establecimiento) {
        this.nombre_establecimiento = nombre_establecimiento;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Intent getDireccion_establecimiento() {
        return direccion_establecimiento;
    }

    public void setDireccion_establecimiento(Intent direccion_establecimiento) {
        this.direccion_establecimiento = direccion_establecimiento;
    }

    public Intent getTelefono_establecimiento() {
        return telefono_establecimiento;
    }

    public void setTelefono_establecimiento(Intent telefono_establecimiento) {
        this.telefono_establecimiento = telefono_establecimiento;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    @Override
    public String toString() {
        return "ResponseEstablishment{" +
                "id=" + id +
                ", nombre_establecimiento='" + nombre_establecimiento + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", direccion_establecimiento=" + direccion_establecimiento +
                ", telefono_establecimiento=" + telefono_establecimiento +
                ", imagen='" + imagen + '\'' +
                ", ubicacion='" + ubicacion + '\'' +
                '}';
    }
}

