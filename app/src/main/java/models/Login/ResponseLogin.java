package models.Login;

public class ResponseLogin {
    String message;
    String info;

    public ResponseLogin(String message, String info) {
        this.message = message;
        this.info = info;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    @Override
    public String toString() {
        return "ResponseLogin{" +
                "message='" + message + '\'' +
                ", info='" + info + '\'' +
                '}';
    }
}
