package models.productos;

import java.util.List;

import models.productos.Products;

public class ResponseProducts {
    String message;
    String info;
    List<Products> establishment;

    public ResponseProducts(String message, String info, List<Products> establishment) {
        this.message = message;
        this.info = info;
        this.establishment = establishment;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public List<Products> getEstablishment() {
        return establishment;
    }

    public void setEstablishment(List<Products> establishment) {
        this.establishment = establishment;
    }

    @Override
    public String toString() {
        return "ResponseProducts{" +
                "message='" + message + '\'' +
                ", info='" + info + '\'' +
                ", establishment=" + establishment +
                '}';
    }
}
