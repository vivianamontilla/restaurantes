package models.productos;

public class Products {
    Integer id;
    String Nombre_Productos;

    public Products(Integer id, String nombre_Productos) {
        this.id = id;
        Nombre_Productos = nombre_Productos;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre_Productos() {
        return Nombre_Productos;
    }

    public void setNombre_Productos(String nombre_Productos) {
        Nombre_Productos = nombre_Productos;
    }
    @Override
    public String toString() {
        return "Products{" +
                "id=" + id +
                ", Nombre_Productos='" + Nombre_Productos + '\'' +
                '}';
    }
}
