package models.categorias;

import java.util.List;

public class Category {
    //Integer id;
    String nombre_categoria;

    List<ResponseCategory>establishment;


    public Category() {
    }

    public Category(String nombre_categoria) {
        this.nombre_categoria = nombre_categoria;
    }

    public String getNombre_categoria() {
        return nombre_categoria;
    }

    public void setNombre_categoria(String nombre_categoria) {
        this.nombre_categoria = nombre_categoria;
    }

    @Override
    public String toString() {
        return "Category{" +
                "nombre_categoria='" + nombre_categoria + '\'' +
                '}';
    }
}
