package interfases;

import java.util.List;

import models.Login.ResponseLogin;
import models.categorias.Category;
import models.categorias.ResponseCategory;
import models.establecimiento.ResponseEstablishment;
import models.productos.ResponseProducts;

import models.recetas.Recipe;
import models.recetas.ResponseRecipe;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiService {

    @GET("users?page=2")
    Call<ResponseCategory> getUser();

    @GET("category")
    Call<List<ResponseCategory>>getCategory();

    @POST("api/category")
    Call<ResponseCategory> crearCategory(@Body Category category);


    @GET("products")
    Call<ResponseProducts>getProducts();

    @GET("Establishment")
    Call<List<ResponseEstablishment>>getEstablishment();


    @GET("auth/login")
    Call<ResponseLogin>getLogin();

     @POST("api/recipe")
     Call<ResponseRecipe>createRecipe(@Body Recipe recipe);


   // @DELETE("api/category")
   // Call<ResponseCategory>deleteCategory();

}
